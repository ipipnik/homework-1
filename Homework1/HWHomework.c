//
//  HWHomework.c
//  Homework1
//
//  Created by Vladislav Grigoriev on 21/09/16.
//  Copyright © 2016 com.inostudio.ios.courses. All rights reserved.
//

#include "HWHomework.h"
#include "stdlib.h"
#include <math.h>

#include "time.h"
#include "string.h"

/*
 * Задание 1. (1 балл)
 *
 * Необходмо реализовать метод, который создаст матрицу и заполнит её случайными числами от -100 до 100.
 *
 * Параметры:
 *      rows - количество строк в матрице;
 *      columns - количество столбцов в матрице;
 *
 * Возвращаемое значение:
 *      Матрица размером [rows, columns];
 */
long** createMatrix(const long rows, const long columns) {
    
    long** resultMatrix;
    
    // Для генерации случайных числе
    srand(time(NULL));
  
    // Выделение памяти под указатели на строки
    resultMatrix = (long**)malloc(rows*sizeof(const long));
    
    // Ввод элементов массива
    for(int i=0; i<rows; i++)  // цикл по строкам
    {
        // Выделение памяти под хранение строк
        resultMatrix[i] = (long*)malloc(columns*sizeof(const long));
        for(int j=0; j<columns; j++)  // цикл по столбцам
        {
            // Диапозон: [-100..100]
            resultMatrix[i][j] = rand() % 201 + (-100);
        }
    }
    
    return resultMatrix;
}
/*
 * Задание 2.
 *
 * Необходимо реализовать метод, котрый преобразует входную строку, состоящую из чисел, в строку состоящую из числительных.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      numberString - входная строка (например: "1", "123.456");
 *
 * Возвращаемое значение:
 *      Обычный уровень сложности (1 балл):
 *             Строка, содержащая в себе одно числительное или сообщение о невозможности перевода;
 *             Например: "один", "Невозможно преобразовать";
 *
 *      Высокий уровень сложности (2 балла):
 *              Строка, содержащая себе числительные для всех чисел или точку;
 *              Например: "один", "один два три точка четыре пять шесть";
 *
 *
 */
char* stringForNumberString(const char *numberString) {
    
    char *stringOfNumerals;
    
    // Флаг присутствия в строке недопустимого символа
    unsigned int incorrectCharacter = 0;
    
    // Необходимое колличество символов
    unsigned int requiredNumberOfSymbols = 0;
    
    // Длина исходной строки
    unsigned int lenghtNumberString = strlen(numberString);
    
    // Подсчет необходимого количества символов для записи в результирующую строку
    for (int i = 0; i<lenghtNumberString; i++){
        switch (numberString[i]) {
            case '1':
                requiredNumberOfSymbols += strlen("один ");
                break;
            case '2':
                requiredNumberOfSymbols += strlen("два ");
                break;
            case '3':
                requiredNumberOfSymbols += strlen("три ");
                break;
            case '4':
                requiredNumberOfSymbols += strlen("четыре ");
                break;
            case '5':
                requiredNumberOfSymbols += strlen("пять ");
                break;
            case '6':
                requiredNumberOfSymbols += strlen("шесть ");
                break;
            case '7':
                requiredNumberOfSymbols += strlen("семь ");
                break;
            case '8':
                requiredNumberOfSymbols += strlen("восемь ");
                break;
            case '9':
                requiredNumberOfSymbols += strlen("девять ");
                break;
            case '0':
                requiredNumberOfSymbols += strlen("ноль ");
                break;
            case '.':
                requiredNumberOfSymbols += strlen("точка ");
                break;
            default:
                // Установление флага некорректного символа
                incorrectCharacter = 1;
                break;
        }
    }
    
    // Проверка кооректности введенных символов.
    // '123.456' , '.456' , '123.' - корректная строка.
    // Если в строке есть символ отличный от ['1','2','3','4','5','6','7','8','9','0','.'] - строка
    // считается некорректной.
    if (incorrectCharacter != 1) {
        // Выделяем память. Умножаем на 2, т.к. используем кириллицу
        stringOfNumerals = (char*) calloc(2*requiredNumberOfSymbols, sizeof(char));
        
        // Собираем результирующую строку
        for (int i = 0; i<lenghtNumberString; i++){
            switch (numberString[i]) {
                case '1':
                    strcat(stringOfNumerals, "один ");
                    break;
                case '2':
                    strcat(stringOfNumerals, "два ");
                    break;
                case '3':
                    strcat(stringOfNumerals, "три ");
                    break;
                case '4':
                    strcat(stringOfNumerals, "четыре ");
                    break;
                case '5':
                    strcat(stringOfNumerals, "пять ");
                    break;
                case '6':
                    strcat(stringOfNumerals, "шесть ");
                    break;
                case '7':
                    strcat(stringOfNumerals, "семь ");
                    break;
                case '8':
                    strcat(stringOfNumerals, "восемь ");
                    break;
                case '9':
                    strcat(stringOfNumerals, "девять ");
                    break;
                case '0':
                    strcat(stringOfNumerals, "ноль ");
                    break;
                case '.':
                    strcat(stringOfNumerals, "точка ");
                    break;
                default:
                    //strcat(stringOfNumerals, "Невозможно преобразовать ");
                    break;
            }
        }
    } else {
        stringOfNumerals = (char*) calloc(2*strlen("Невозможно преобразовать "), sizeof(char));
        strcat(stringOfNumerals, "Невозможно преобразовать ");
        
        // Сброс флага некорректного символа
        incorrectCharacter = 0;
    }

    return stringOfNumerals;
}


/*
 * Задание 3.
 *
 * Необходимо реализовать метод, который возвращает координаты точки в зависимости от времени с начала анимации.
 * Задание имеет два уровня сложности: обычный и высокий.
 *
 * Параметры:
 *      time - время с начала анимации в секундах (например: 0 или 1.234);
 *      canvasSize - длина стороны квадрата, на котором происходит рисование (например: 386);
 *
 * Возвращаемое значение:
 *      Структура HWPoint, содержащая x и y координаты точки;
 *
 * Особенность: 
 *      Начало координат находится в левом верхнем углу.
 *
 * Обычный уровень сложности (2 балла):
 *      Анимация, которая должна получиться представлена в файле Default_mode.mov.
 *
 * Высокий уровень сложности (3 балла):
 *      Анимация, которая должна получиться представлена в файле Hard_mode.mov.
 *
 * PS: Не обязательно, чтобы ваша анимация один в один совпадала с примером. 
 * PPS: Можно реализовать свою анимацию, уровень сложности которой больше или равен анимациям в задании.
 */
HWPoint pointForAnimationTime(const double time, const double canvasSize) {
    
    // p = (a/2pi)*phi - Уравнение Архимедовой спирали
    // a - Шаг спирали
    // phi - Параметр угла движения
    
    const double a  = 45.0;
    static double phi = 0.0;
    
    // Координаты спирали
    double x = ((a / (2.0 * M_PI))  * phi * cos(phi)) + (canvasSize / 2.0);
    double y = ((a / (2.0 * M_PI))  * phi * sin(phi)) + (canvasSize / 2.0);
    
    // Проверка достижения границы отрисовки
    if((x < 0.0 || x >= canvasSize) || (y < 0.0 || y >= canvasSize))
        phi = -phi; // Меняем направление
    
    // Положительным значениям phi - соответствует правая спираль, отрицательным — левая спираль
    phi -= 0.05;
    
    return (HWPoint){x,  y};
}

